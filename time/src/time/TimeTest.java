package time;
import static org.junit.Assert.*;

import org.junit.Test;
/**
 * @author Divjot Chawla
 */

public class TimeTest {

   @Test
   public void testGetMillisecondsRegular()
   {
	   int getTotalMilliseconds= Time.getTotalMilliseconds("12:05:05:05");
	  assertTrue("Invalid milliseonds", getTotalMilliseconds==5);
   }
   
   @Test (expected=NumberFormatException.class)
	 public void testGetTotalMillisecondsException() {
		 int getTotalMilliseconds= Time.getTotalMilliseconds("01:01:01:0A");
		 fail (" The time provided is not valid");
   }
   
   @Test
   public void testGetTotalMillisecondsBoundaryIn() {
		 int getTotalMilliseconds= Time.getTotalMilliseconds("01:01:01:999");
		 assertTrue (" The time provided does not match the result", getTotalMilliseconds == 999);
	 }
   
   @Test (expected=NumberFormatException.class)
	 public void testGetTotalMillisecondsBoundaryOut() {
		 int getTotalMilliseconds= Time.getTotalMilliseconds("01:01:01:1000");
		 fail ("The milliseconds provided are Invalid");
	 }
   
   
   
   
	/*  
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		assertTrue( "The time provided does not match the result", totalSeconds==3661);
	}

	 @Test (expected=NumberFormatException.class)
	 public void testGetTotalSecondsException() {
		 int totalSeconds= Time.getTotalSeconds("01:01:0A");
		 fail (" The time provided is not valid");
	 }
	 
	 @Test 
	 public void testGetTotalSecondsBoundaryIn() {
		 int totalSeconds= Time.getTotalSeconds("00:00:59");
		 assertTrue (" The time provided does not match the result", totalSeconds == 59);
	 }
	 
	 @Test (expected=NumberFormatException.class)
	 public void testGetTotalSecondsBoundaryOut() {
		 int totalSeconds= Time.getTotalSeconds("01:01:60");
		 fail ("The time provided is not valid");
	 }
	 */
}
